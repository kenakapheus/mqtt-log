#!/usr/bin/python3

import paho.mqtt.client as mqtt
import time
import gzip
import argparse
import os.path
import urllib.parse

parser = argparse.ArgumentParser(description='Log MQTT Messages to gzip compressed files')
parser.add_argument('--host', default='mqtt', help='MQTT server address. Defaults to "mqtt"')
parser.add_argument('--topic', '-t',  default='#', help='Topic to subscribe to. Default is "#"')
parser.add_argument('--log-dir', '-d', default="", help='Path to logging directory. Defaults to the running dir.')
parser.add_argument('--log-interval', '-i', default="60", type=int, help='Time in minutes to wait until a new file is created. Defauts to 60. 0 disables creating new files.')

args=parser.parse_args()

logfile = None

def newFile():
	global logfile
	filename = os.path.join(args.log_dir,time.strftime("%Y%m%d_%H%M%S_mqtt.log.gz"))
	print("Opening '%s' for writing." % filename)
	if logfile:
		logfile.close()
	logfile = gzip.open(filename, 'w')

def log_put(key, val, timestamp):
	global logfile
	logline = "%f,%s,%s\n" % (timestamp, key, urllib.parse.quote_plus(val))
	logfile.write(logline.encode("utf8"))

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))
	client.subscribe([(args.topic, 0)])

def on_message(client, userdata, msg):
	log_put(msg.topic, msg.payload, time.time())

client = mqtt.Client()

client.on_connect = on_connect
client.on_message = on_message

client.connect(args.host)

newFile()

try:
  if args.log_interval:
    client.loop_start()
    while True:
      time.sleep(60*args.log_interval)
      newFile()
  else:
    client.loop_forever()
except KeyboardInterrupt:
  if logfile:
    logfile.close()
    exit(0)

