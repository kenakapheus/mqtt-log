#!/usr/bin/python3

import vcd
import gzip
import sys
import csv
import time
import urllib.parse

def isBool(val):
	return (val == "True" or val == "true" or val == "False" or val == "false")

def toBool(val):
	return (val == "True" or val == "true")

variables = {}
firstTs = False

with vcd.VCDWriter(open(sys.argv[1] + ".vcd", "w"), timescale='1 ms') as writer:
	logfile = csv.reader(gzip.open(sys.argv[1], "rt"), delimiter=',')
	for timestamp, topic, val in logfile:
		stateVal = urllib.parse.unquote_plus(val)
		if topic not in variables:
			subtopics = topic.split("/")
			name = subtopics[-1]
			module = ".".join(subtopics[:-1])
			if isBool(stateVal):
				variables[topic] = writer.register_var(module, name, 'reg', size=1)
			else:
				try:
					t = int(stateVal)
					variables[topic] = writer.register_var(module, name, 'integer', size=64)
				except:
					try:
						t = float(stateVal)
						variables[topic] = writer.register_var(module, name, 'real', size=64)
					except:
						variables[topic] = writer.register_var(module, name, 'string')
					
				
				
	logfile = csv.reader(gzip.open(sys.argv[1], "rt"), delimiter=',')
	for timestamp, topic, val in logfile:
		stateVal = urllib.parse.unquote_plus(val)
		ts = int(float(timestamp)*1000)
		if not firstTs:
			firstTs = ts
		ts = ts - firstTs
		if isBool(stateVal):
			writer.change(variables[topic], ts, toBool(stateVal))
		else:
			try:
				t = int(stateVal)
				writer.change(variables[topic], ts, t)
			except:
				try:
					t = float(stateVal)
					writer.change(variables[topic], ts, t)
				except:
					writer.change(variables[topic], ts, stateVal)
		print(ts, topic, stateVal)
		


	
