#!/usr/bin/python3

import os
import gzip
import shutil
import sys

if len(sys.argv) > 1:
  fileList = sys.argv[1:]
else:
  fileList = []
  files = os.listdir()
  for f in files:
    if ".log.gz" in f and "all" not in f:
      fileList += [f]

fileList.sort()

print(fileList)

with gzip.open('all.log.gz', 'w') as f_out:
	for f in fileList:
		try:
			with gzip.open(f, 'r') as f_in:
				f_out.write(f_in.read())
		except:
			pass
			print(f)
